Some example code!
===========

## V1

This was the original program that was written while I was still learning Python. It has a variety of "features" such as: 
 1. At the time I didn't know that Python had built-in logging so I wrote my own. 
 2. Because it has threads it needed semaphores so the log file wouldn't get messed up. 
 3. Really interesting ways to parse the html output (no API) from the streaming server
 
The only files of interest are bp2.py and Watchdir.py. The rest are an accumulation of little helper scripts to do certain things.

## V2

A cleaner version that will eventually take the place of the other version. Refactored and kept with a common convention 
PEP-8 (mostly) in the code. Cleaned up the interfaces and relations between the components.
