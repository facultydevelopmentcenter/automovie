"""
@Author Robert Powell
@Version 1.0.0

Yahp
"""

import Tkinter as tk
import ttk

import Factory
import AutoMovie

import math

class ResultRow(object):
    ''' Container for each row in results row '''

    def __init__(self, parent, program):

        self.program = program
        self.name_label = ttk.Label(parent, width=35)
        self.channel_box = ttk.Combobox(parent, state='readonly', width=20)
        self.link_label = ttk.Label(parent, width=60)
        self.update_button = ttk.Button(parent)

        self._render()

    def _render(self):
        ''' Paint internal frame to tkinter '''

        self.name_label['text'] = self.program.name

        self.channel_box['values'] = [self.program.channel]
        self.channel_box.current(0)

        self.link_label['text'] = self.program.guid
        self.update_button['text'] = 'Update'

class Navigation(ttk.Frame):
    ''' Container for navigation controls on results '''

    def __init__(self, parent, programs):

        ttk.Frame.__init__(self, parent)
        self.parent = parent

        self.programs = programs

        # pages
        self.count = 0
        self.step = 20
        self.pages = math.ceil(max(len(self.programs)/float(self.step), 1))
        self.page = 1
        self.current = tk.StringVar()
        self.last = tk.StringVar()
        self.current_page = ttk.Label(self, textvariable=self.current)
        self.last_page = ttk.Label(self, textvariable=self.last)
        self.count_size = len(self.programs)

        #buttons
        self.previous_button = ttk.Button(self)
        self.next_button = ttk.Button(self)

        # Placing static settings in constructor
        self.next_button['text'] = 'Next'
        self.previous_button['text'] = 'Back'

       	self._render()

    def update(self):
        ''' Update internal values based on new program data '''

        self.count = 0
        self.pages = math.ceil(max(len(self.programs)/float(self.step), 1))
        self.page = 1
        self.count_size = len(self.programs)
        self._render()

    def getResultList(self):
        ''' Return list of programs currently in view '''

        return self.programs[self.count:self.count+self.step]

    def nextResults(self, event):
        ''' Increment what programs are on the page '''

    	self.count += self.step
        self.page += 1
    	self._render()
    	self.parent._render()

    def previousResults(self, event):
        ''' Decrement what programs are on the page '''

    	self.count -= self.step
        self.page -= 1
    	self._render()
    	self.parent._render()

    def _render(self):
        ''' Draw the current state of the navigation class ''' 

        self.current.set("Page "+str(self.page)+" of")
        self.current_page.config(font=('times', 16, 'bold'))
        self.current_page.pack(side=tk.LEFT, fill=tk.X)

        self.last.set(int(self.pages))
        self.last_page.config(font=('times', 16, 'bold'))
        self.last_page.pack(side=tk.LEFT, fill=tk.X)

        self.previous_button.pack(side=tk.LEFT, fill=tk.X)
        self.next_button.pack(side=tk.LEFT, fill=tk.X)

        # if on first/last page
        if self.page <= 1:
            self.previous_button.config(state=tk.DISABLED)
            self.previous_button.unbind("<Button-1>")
        else:
            self.previous_button.config(state=tk.NORMAL)
            self.previous_button.bind("<Button-1>", self.previousResults)
        if self.page >= self.pages:
            self.next_button.config(state=tk.DISABLED)
            self.next_button.unbind("<Button-1>")
        else:
            self.next_button.config(state=tk.NORMAL)
            self.next_button.bind("<Button-1>", self.nextResults)

class ResultsPane(ttk.Labelframe):
    ''' Container for sorted data from BurstPoint '''

    def __init__(self, parent):

        ttk.LabelFrame.__init__(self, parent, text='Results')
        self.parent = parent

        self.scroll_frame = ttk.Frame(self)

        self.nav = Navigation(self, self.parent.programs)

        self.scroll_frame.pack(side=tk.TOP, fill=tk.X)
        self.nav.pack(side=tk.BOTTOM, fill=tk.X)

        self._render()

    def update(self):
        ''' Update Navigation object with new program data '''
        self.nav.update()

    def _render(self):
        ''' Render widget to the screen '''

        for widget in self.scroll_frame.winfo_children():
            widget.grid_forget()

        for i, program in enumerate(self.nav.getResultList()):
            row = ResultRow(self.scroll_frame, program)
            row.name_label.grid(column=0, row=i, sticky=tk.W)
            row.channel_box.grid(column=1, row=i, sticky=tk.W)
            row.link_label.grid(column=2, row=i, sticky=tk.W)
            row.update_button.grid(column=3, row=i, stick=tk.W)


class FilterPane(ttk.LabelFrame):
    ''' Element within BrowseWindow to contain filter options '''

    def __init__(self, parent):

        ttk.LabelFrame.__init__(self, parent, text='Filter')

        self.parent = parent

        self.type_filter = ttk.Combobox(self)
        self.selection_filter = ttk.Combobox(self, state='readonly')
        self.var = tk.StringVar()
        self.is_event = ttk.Checkbutton(self, variable=self.var)
        self.search_box = ttk.Entry(self)

        self._render()

    def filterName(self, event):
        ''' Filter program list based on selected channel '''

        # Edit the program array in place to keep references
        self.parent.programs[:] = self.parent.programs_static[:]

        if self.selection_filter.get() != "All":
            for p in reversed(self.parent.programs):
                if p.channel != self.selection_filter.get():
                    self.parent.programs.remove(p)
        self.parent.update_results()

    def _render(self):
        ''' Paint the initial buttons '''

        self.type_filter['values'] = ['Programs', 'Library']
        self.type_filter.current(0)
        self.type_filter.pack(side=tk.LEFT)

        arr = ["All"]
        arr += [ch.name for ch in self.parent.channels]

        self.selection_filter['values'] = arr
        self.selection_filter.current(0)
        self.selection_filter.pack(side=tk.LEFT)
        self.selection_filter.bind("<<ComboboxSelected>>", self.filterName)

        self.is_event['text'] = 'Live Events'
        self.is_event.pack(side=tk.LEFT)

        self.search_box.pack(side=tk.RIGHT)


class BrowseWindow(ttk.Frame):
    ''' Container for the Browse tab in the Notebook '''

    def __init__(self, parent, bp):

        ttk.Frame.__init__(self, parent, height=500, width=750)

        self.bp = bp
        self.programs_static = bp.get_all_programs()
        self.programs = self.programs_static[:]
        self.channels = bp.get_all_channels()
        self.filter_pane = FilterPane(self)
        self.results_pane = ResultsPane(self)

        self._render()

    def update_results(self):
        ''' Pass through to update the contained widgets '''

        self.results_pane.update()
        self.results_pane._render()

    def _render(self):
        ''' Pain the compenents to the screen '''

        self.filter_pane.pack(side=tk.TOP, fill=tk.X)
        self.results_pane.pack(side=tk.TOP, fill=tk.X)


class CreateWindow(ttk.Panedwindow):
    ''' Container for the Browse tab in the Notebook '''

    def __init__(self, parent):

        ttk.Panedwindow.__init__(self, parent, orient=tk.VERTICAL, height=650, width=750)


class ApplicationNotebook(ttk.Notebook):
    ''' Container for the two main application views '''

    def __init__(self, parent, bp):

        ttk.Notebook.__init__(self, parent)
        self.browse_tab = BrowseWindow(self, bp)
        self.create_tab = CreateWindow(self)
        self.add(self.browse_tab, text='Browse')
        self.add(self.create_tab, text='Create')


class Main(ttk.Frame):
    ''' Toplevel controller for our ASS gui '''

    def __init__(self, parent):

        ttk.Frame.__init__(self, parent)
        self.parent = parent
        self.auto_movie = AutoMovie.AutoMovie()
        self.auto_movie.sync()

        self.notebook = ApplicationNotebook(self, self.auto_movie)
        self.notebook.pack(side='top', fill='both')


if __name__ == "__main__":
    root = tk.Tk()
    Main(root).pack(side="top", fill="both", expand=True)
    root.mainloop()
