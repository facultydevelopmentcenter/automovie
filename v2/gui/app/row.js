import React from "react";

class row extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (   
            <div>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Channel/Folder</th>
                        <th>Link</th>
                        <th>Options</th>
                    </tr>
                    <tr>
                        <td>{this.props.name}!</td>
                        <td>{this.props.name}!</td>
                        <td>{this.props.name}!</td>
                        <td>{this.props.name}!</td>
                    </tr>
                </table>
            </div>
            );
    }
}

export default row;
