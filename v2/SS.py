"""
@Author Robert Powell
@Version 2.0.0

Interface to the database containing links to Burstpoint information
"""

import AutoMovie
import Factory
import sqlite3
import logging

class Adapter(object):
    ''' Interface to a particular database '''

    def __init__(self, db):

        self.db = db
        self.conn = sqlite3.connect(db)
        self.c = self.conn.cursor()

    def dump_programs(self):
        ''' Return a list of program objects from database '''

        results = self.c.execute('SELECT * FROM Programs')

        for row in results:
            yield Factory.Program(row[0], row[1], row[2], row[3])

    def update_programs(self, programs):
        ''' Update the entire program list from Burstpoint '''

        update_list = [(p.name, p.channel, p.guid, p.id) for p in programs]
        self.c.execute('DELETE from Programs')
        self.c.executemany('INSERT INTO Programs VALUES (?,?,?,?)', update_list)
        self.conn.commit()

    def dump_channel(self):
        ''' Return a list of channel objects from database '''

        results = self.c.execute('SELECT * FROM Channels')

        for row in results:
            yield Factory.Channel(row[0], row[1])

    def update_channels(self, channels):
        ''' Update the entire channel list from Burstpoint '''

        update_list = [(c.name, c.id) for c in channels]
        self.c.execute('DELETE from Channels')
        self.c.executemany('INSERT INTO Channels VALUES (?,?)', update_list)
        self.conn.commit()

    def dump_files(self):
        ''' Return a list of file objects from database '''

        results = self.c.execute('SELECT * FROM Files')

        for row in results:
            yield Factory.LibraryFile(row[0], row[2], row[1])

    def update_files(self, files):
        ''' Update the entire file list from Burstpoint '''

        update_list = [(f.name, f.id) for f in files]
        self.c.execute('DELETE from Files')
        self.c.executemany('INSERT INTO Files VALUES (?,-1,?)', update_list)
        self.conn.commit()

    def dump_folders(self):
        ''' Return a list of folder objects from database '''

        results = self.c.execute('SELECT * FROM Folders')

        for row in results:
            yield Factory.Folder(row[0], row[1])

    def update_folders(self, folders):
        ''' Update the entire folder list from Burstpoint '''

        update_list = [(f.name, f.id) for f in folders]
        self.c.execute('DELETE from Folders')
        self.c.executemany('INSERT INTO Folders VALUES (?,?)', update_list)
        self.conn.commit()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    test = AutoMovie.AutoMovie()
    adapter = Adapter('./data/bp.db')
    test.sync()

    programs = test.get_all_programs()
    channels = test.get_all_channels()
    files = test.get_all_files()
    folders = test.get_all_folders()

    adapter.update_programs(programs)
    adapter.update_files(files)
    adapter.update_folders(folders)
    adapter.update_channels(channels)

    program_results = adapter.dump_programs()
