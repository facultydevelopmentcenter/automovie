"""
@Author Robert Powell
@Version 1.0.0

Command line runner for the AutoMovie functionality
"""

import AutoMovie
import Utilities
import argparse

class Command(object):
    ''' Abstraction to contain a currently running command '''

    def __init__(self, source_folder, channel):

        self.automovie = AutoMovie.AutoMovie()
        self.file_manager = Utilities.FileManager(source_folder)
        self.channel = channel
    
    def bootstrap(self):
        ''' Get updated local copy of BurstPoint '''

        self.automovie.sync()
        self.set_working_videos()

    def set_working_videos(self):
        ''' Get a complete list of working videos '''

        self.file_manager.stage_videos()

    def push_videos_to_burstpoint(self):
        ''' Get our working mp4s to Burstpoint '''

        videos = Factory.get_video_objects(self.file_manager.mp4s)

        # TODO Move it into the correct folder instead of vodpub
        folder = self.automovie.get_folder_by_name('vodpub')
        channel = self.automovie.get_channel_by_name(self.channel)


        for video in videos:
            self.automovie.create_new_video(video, channel, folder) 


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'A better way to deal with Burstpoint!')
    parser.add_argument('folder', type = str, help = 'Folder containing videos to work with')
    parser.add_argument('-c', '--channel', type = str, help = 'Burstpoint channel to work with')
    parser.add_argument('-w', '--watch', action = 'store_true', help = 'Watch the folder for changes')
    args = parser.parse_args()
    if (args.watch):
        auto = Command(args.folder, args.channel)
        auto.bootstrap()
        auto.push_videos_to_burtpoint()
