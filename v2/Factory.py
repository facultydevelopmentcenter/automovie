"""
@Author Robert Powell
@Version 2.0.0

Helper module for working with the BP interface
"""

class Channel(object):
    ''' Container for the Burstpoint channel data '''

    def __init__(self, name, channel_id):

        self.name = name
        self.id = channel_id
        self.programs = []

class Folder(object):
    ''' Container for the Burstpoint library data '''

    def __init__(self, name, folder_id):

        self.name = name
        self.id = folder_id
        self.files = []

class Program(object):
    ''' A container for BurstPoint programs '''

    def __init__(self, name, channel, guid, vid_id):

        self.name = name
        self.channel = channel
        self.id = vid_id
        self.guid = guid

class LibraryFile(object):
    ''' A container for BurstPoint library files '''

    def __init__(self, name, resource_id, folder):

        self.name = name
        self.id = resource_id
        self.programs = []

    def get_programs(self):
        ''' Return the list of associated programs '''

        return self.programs

class Video(object):
    ''' A container for video data '''

    def __init__(self, name, path):

        self.name = name
        self.path = path

class Factory(object):
    ''' Factory for creating objects from raw BP data '''

    @classmethod
    def generate_library_objects(cls, file_array):
        ''' Create and return a list of LibraryFile objects '''

        library_objects = []
        for lib_file in file_array:
            library_objects.append(LibraryFile(lib_file[0], lib_file[1], lib_file[2]))

        return library_objects

    @classmethod
    def generate_programs_objects(cls, program_array):
        ''' Create and return a list of Program objects '''

        program_objects = []
        for prog_file in program_array:
            program_objects.append(Program(prog_file[0], prog_file[1], prog_file[2], prog_file[3]))

        return program_objects

    @classmethod
    def generate_video_objects(cls, video_array, name, path):
        ''' Create and return a list of Video objects '''

        video_objects = []
        for video_file in video_array:
            video_objects.append(Video(os.path.basename(path), path))

        return video_objects

    @classmethod
    def generate_folder_objects(cls, folder_array):
        ''' Create and return a list of channels '''

        folder_objects = []
        for folder in folder_array:
            folder_objects.append(Folder(folder[0], folder[1]))

        return folder_objects

    @classmethod
    def generate_channel_objects(cls, channel_array):
        ''' Create and return a list of channels '''

        channel_objects = []
        for channel in channel_array:
            channel_objects.append(Channel(channel[0], channel[1]))

        return channel_objects
