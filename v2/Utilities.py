"""
@Author Robert Powell
@Version 1.0.0

Local file management for use within AutoMovie
"""

import os
import subprocess
import tempfile
import shutil
import logging

class FileManager(object):
    ''' Quick functions to work with files on computer '''

    extensions = ('.avi', '.wmv', '.mov', '.mpg', '.flv')

    def __init__(self, folder):

        self.folder = folder
        self.mp4s = []
        self.other = []
        self.temp_dir = False

    def stage_videos(self):
        ''' Setup a structure for working with found videos '''

        self.mp4s, self.other = self.get_video_files(self.folder)

        for mp4_video in self.mp4s:
            self._copy_to_temp_folder(mp4_video)

        for other_video in self.other:
            converted_video = os.path.join(self.temp_dir, os.path.basename(video))
            self.convert_to_mp4(other_video, converted_video)

            # Mark the fact that we've got some new mp4s
            self.mp4s.append(converted_video)

    def get_video_files(self, folder):
        ''' Check instance folder of video files '''

        files = []
        mp4s = []
        other = []

        for (dirpath, _, filenames) in os.walk(folder):
            files.extend(os.path.join(dirpath, filenames))
            break

        for filename in files:
            if filename.endswith(FileManager.extensions):
                other.append(filename)
            elif filename.endswith('.mp4'):
                mp4s.append(filename)

        return (mp4s, other)

    def _set_temp_dir(self):
        ''' Set an internal temporary directory '''

        self.temp_dir = tempfile.mkdtemp()

    def _clean_temp_dir(self):
        ''' Delete the created temporary directory '''

        os.rmdir(self.temp_dir)

    @classmethod
    def convert_to_mp4(cls, source, dest):
        ''' Convert the supplied file to an mp4 video '''

        command = "/Applications/HandBrakeCLI -i '{}' -o '{}' -f mp4 -O -e x264 -b 700 -2 -T --vfr"
        command = command.format(source, dest)

        try:
            # Tried turning off shell=True for now
            subprocess.check_call(command)
        except:
            # Do something about this later
            pass

    def _copy_to_temp_folder(self, source):
        ''' Copy a in-progress file to the temporary folder '''

        if self.temp_dir:
            shutil.move(source, self.temp_dir)
        else:
            self._set_temp_dir()
            shutil.move(source, self.temp_dir)

        return os.path.join(self.temp_dir, os.path.basename(source))
