"""
@Author Robert Powell
@Version 2.0.0

Automatic interface to BurstPoint for a better life
"""
import time
import logging
import Burstpoint
from Factory import *


class AutoMovie(object):
    ''' That magical interface '''

    def __init__(self):

        self.folders = []
        self.channels = []
        self.programs = []
        self.files = []
        self.burstpoint = Burstpoint.BP()

    def sync(self):
        ''' Establish the link between AutoMovie and BP '''

        self.programs = self.get_all_programs()
        self.files = self.get_all_files()
        self.channels = self.get_all_channels()
        self.folders = self.get_all_folders()

    def refresh_programs(self):
        ''' Download the latest program list from BP '''

        self.programs = self.get_all_programs()

    def refresh_library(self):
        ''' Download the lastest library list from BP '''

        self.files = self.get_all_files()

    def get_all_channels(self):
        ''' Return a list of all Channels on Burstpoint '''

        self.burstpoint.login()
        raw_channels = self.burstpoint.download_channels()
        channels = Factory.generate_channel_objects(raw_channels)
        print len(channels)
        return channels

    def get_all_folders(self):
        ''' Return a list of all Folders on Burstpoint '''

        self.burstpoint.login()
        raw_folders = self.burstpoint.download_folders()
        folders = Factory.generate_folder_objects(raw_folders)
        return folders

    def get_all_files(self):
        ''' Return a list of all LibraryFiles on Burstpoint '''

        self.burstpoint.login()
        raw_files = self.burstpoint.download_library()
        library_files = Factory.generate_library_objects(raw_files)
        return library_files

    def get_all_programs(self):
        ''' Return a list of all Programs on Burstpoint '''

        self.burstpoint.login()
        raw_programs = self.burstpoint.download_programs()
        raw_events = self.burstpoint.download_live_events()
        event_files = Factory.generate_programs_objects(raw_events)
        program_files = Factory.generate_programs_objects(raw_programs)
        return program_files + event_files

    def get_channel_by_name(self, name):
        ''' Return a channel object based off name '''

        for channel in self.channels:
            if channel.name == name:
                return channel

    def get_folder_by_name(self, name):
        ''' Return a folder object based off name '''

        for folder in self.folders:
            if folder.name == name:
                return folder

    def get_programs_by_channel(self, channel):
        ''' Return a list of programs of a channel (iterator) '''

        for program in self.programs:
            if program.channel == channel.name:
                yield program

    def _check_for_duplicate(self, file_name):
        ''' Look for a file of duplicate name in the BP library '''

        duplicates = [f for f in self.files if f.name == file_name]
        if len(duplicates) > 0:
            return (True, duplicates[0])
        else:
            return (False, False)

    def _check_for_program(self, program_name):
        ''' Search for an exact match on program title '''

        matches = [p for p in self.programs if p.name == program_name]
        if len(matches) > 0:
            return (True, matches)
        else:
            return (False, False)

    def upload_new_file(self, video, folder):
        ''' Upload a file to the burstpoint library '''

        (result, new_file) = self._check_for_duplicate(video.name)
        if result:
            logging.warning('Found duplicate file ' + video.name)
            logging.warning('Failed to upload file ' + video.path)
            return False
        else:
            self.burstpoint.create_file(video, folder)
            self.refresh_library()

        # Make sure it uploaded by checking for a duplicate
        (result, new_file) = self._check_for_duplicate(video.name)
        if result:
            logging.info('Uploaded file ' + video.path)
            return new_file
        else:
            logging.warning('Failed to upload file ' + video.path)
            return False

    def create_new_program(self, library_file, channel, folder):
        ''' Create a new VOD program on Burstpoint from a library file '''

        self.burstpoint.create_program(library_file, channel, folder)

        # This is stupid, but we have to give BP time to refresh
        time.sleep(10)
        # At some point fix it

        self.refresh_programs()
        (result, programs) = self._check_for_program(library_file.name)
        if result:
            logging.info('Created new program ' + library_file.name)
            return programs[0]
        else:
            logging.warn('Failed to create new program ' + library_file.name)
            return False

    def create_new_video(self, video, channel, folder):
        ''' Create a new LibraryFile and Program on Burstpoint '''

        library_file = self.upload_new_file(video, folder)
        if library_file == False:
            logging.warn('Failed to create a new video on BP ' + video.path)
            return False
        else:
            program = self.create_new_program(library_file, channel, folder)
            if program == False:
                logging.warn('Failed to create a new video on BP ' + video.path)
                return False

        return True

    def update_video(self, program, new_channel):
        ''' Change the channel assoicated with a video '''

        self.burstpoint.update_program_channel(program, new_channel)

        return True


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    test = AutoMovie()
    test.sync()

    programs = test.get_programs_by_channel(test.get_channel_by_name("Robert_Powell"))

    for program in programs:
        test.burstpoint.download_media_file(program, '/tmp/' + program.name + '.mp4')
        time.sleep(2)

