"""
@Author Robert Powell
@Version 2.0.0

Module for interfacing with the burstpoint streaming server
"""

from Factory import Channel, Program
from poster.encode import multipart_encode
from poster.streaminghttp import StreamingHTTPHandler, StreamingHTTPRedirectHandler, StreamingHTTPSHandler
import lxml.html
import urllib
import urllib2
import cookielib
import logging
import json
import time
import hashlib


def create_guid():
    ''' Generate a GUID based off the current time '''

    guid = hashlib.sha1()
    guid.update(str(time.time()))
    return guid.hexdigest()[:32].upper()

class BP(object):
    ''' Interface to navigate Burstpoint '''

    def __init__(self):

        handlers = [StreamingHTTPHandler, StreamingHTTPRedirectHandler, StreamingHTTPSHandler]

        self.username = 'fdcit'
        self.password = 'PWfdcit#1'
        self.cookie = cookielib.CookieJar()
        self.opener = urllib2.build_opener( \
                urllib2.HTTPCookieProcessor(self.cookie), \
                *handlers)
        urllib2.install_opener(self.opener)

    def login(self):
        ''' Log into BP and store auth cookie '''

        login_data = urllib.urlencode({'_spring_security_remember_me':'true', \
                        'j_username':self.username, \
                        'j_password':self.password})

        url = 'http://130.111.228.122/j_spring_security_check'
        self.opener.open(url, login_data)

        logging.info('Authenticated to BP')

    def _download_page(self, url, data=None):
        ''' Given a BP url download the raw html '''

        if data:
            page = self.opener.open(url, data)
        else:
            page = self.opener.open(url)

        raw = page.read()
        dom = lxml.html.document_fromstring(raw)

        return dom

    def download_library(self):
        ''' Download the BP library files '''

        url = 'http://130.111.228.122/create_library.html?pStart=25&pNum=99999'
        dom = self._download_page(url)

        library_table = dom.xpath("""//tr[@class="tableEvenRow txtMedium" or
                                    @class="tableOddRow txtMedium"]""")

        library_files = []
        for element in library_table[1:-1]:
            file_id = element.xpath('td[position()=1]/input')[0].attrib['value']
            file_name = element.xpath('td[position()=2]')[0].text
            folder_name = element.xpath('td[position()=3]')[0].text
            library_files.append((file_name, file_id, folder_name))

        logging.info('Downloaded BP library files')
        return library_files

    def download_programs(self):
        ''' Download all programs listed on BP '''

        url = 'http://130.111.228.122/create_allPrograms_programList.html?pStart=-1'
        dom = self._download_page(url)

        video_table = dom.xpath("""//tr[@class="tableEvenRow txtMedium" or
                                @class="tableOddRow txtMedium"]""")

        video_tuples = []
        for element in video_table[0:-1]:
            vid_id = element.xpath('td[position()=1]/input')[0].attrib['value']
            title = element.xpath('td[position()=2]')[0].text
            faculty = element.xpath('td[position()=4]')[0].text
            link = element.xpath('td[position()=7]/a[@href]')[0].attrib['href']
            video_tuples.append((title, faculty, link, vid_id))

        logging.info('Downloaded BP programs')
        return video_tuples

    def download_live_events(self):
        ''' Download all live events from BP '''

        url = 'http://130.111.228.122/create_allPrograms.html?pStart=-1&programType=liveevents'

        dom = self._download_page(url)

        video_table = dom.xpath("""//tr[@class="tableEvenRow txtMedium" or
                                @class="tableOddRow txtMedium"]""")

        video_tuples = []
        for element in video_table[0:-1]:
            vid_id = element.xpath('td[position()=1]/input')[0].attrib['value']
            title = element.xpath('td[position()=2]')[0].text
            faculty = element.xpath('td[position()=4]')[0].text
            try:
                link = element.xpath('td[position()=7]/a[@href]')[0].attrib['href']
                video_tuples.append((title, faculty, link, vid_id))
            except:
                pass

        logging.info('Downloaded BP live events')
        return video_tuples

    def download_channels(self):
        ''' Download the program channel id -> faculty table '''

        url = 'http://130.111.228.122/create_allPrograms.html?pStart=-1'
        dom = self._download_page(url)

        raw_ids = dom.xpath("""//div[@class="catTree"]/script""")[0].text
        raw_ids = raw_ids.split(';')[0]
        raw_ids = raw_ids[26:]
        json_ids = json.loads(raw_ids)

        faculty_tuples = []
        for faculty in json_ids['items'][6]['children']:
            faculty_tuples.append((faculty['name'], faculty['id'][3:]))

        logging.info('Downloaded channels')
        return faculty_tuples

    def download_folders(self):
        ''' Download the library folder id -> faculty '''

        url = 'http://130.111.228.122/create_library.html'
        dom = self._download_page(url)

        folder_tuples = []
        select_options = dom.xpath("""//select[@id="selfolderId"]/option""")
        for folder in select_options:
            folder_tuples.append((folder.text, folder.get('value')))

        logging.info('Downloaded Library folders')
        return folder_tuples

    def create_file(self, video, folder):
        ''' Upload a video file to the library '''

        url = 'http://130.111.228.122/create_library.html'

        payload = {
            'resourceId' : '',
            'mediaType': 'videos',
            'oldSelFolderId': '',
            'resourceFile': open(video.path, 'rb'),
            'fileUpload': 'true',
            'resourceDisplayName': video.name,
            'folderId': folder.id,
            'resourceDescription': '',
            'resourceAccessLevel': '1'
        }

        datagen, headers = multipart_encode(payload)
        request = urllib2.Request(url, datagen, headers)
        print self.opener.open(request).read()

    def create_program(self, library_file, channel, folder):
        ''' Create a BurstPoint program from a library file '''

        url = 'http://130.111.228.122/create.html'
        guid = create_guid()

        post_data = {
            'programGuid': guid, #Need to create GUID
            'categoryId': '11',
            'programThumbnailId': '2439',
            'channelId': channel.id, #'128' FDC_Crew
            'programTitle': library_file.name, 
            'description': '',
            'themeId': '8',
            'programUrl': 'http://bp-manager.ume.maine.edu/viewResource.html?guid=+' + guid,
            'programVisibility': '1',
            'videoId': library_file.id,
            'slidesetId': '',
            'attachmentIds': '',
            'playlistData': '',
            'selfolderId': folder.id, #'117' FDC folder
            'programLinks': '',
            'rebroadcastData': '',
            'rebroadcastTime': '',
            'reboradcastRepeatMode': '0',
            'rebroadcastRepeatCount': '',
            'publicEvent': 'true',
            'specialPassword': '',
            'specialPasswordConfirm': '',
            'step': '4',
            'programType': '1',
            'programTemplateId': '1',
            'publish': 'true',
            'dojo.preventCache': '1437666170863'
        }

        data = urllib.urlencode(post_data)
        self.opener.open(url, data)

    def update_program_channel(self, program, new_channel):
        ''' Update a program's channel on Burstpoint '''

        url = 'http://130.111.228.122/create_allPrograms_programList.html'
        data = {
            'preventCache': '1438269782822',
            'programType': 'vod',
            'thumbnailId': '',
            'programId': program.id,
            'programName': program.name,
            'description': '',
            'programCategoryId': 11,
            'programChannelId': new_channel.id,
            'themeName': 'MP4_Video_Only',
            'programURL': 'http://bp-manager.ume.maine.edu/viewResource.html?guid='+program.guid,
            'programVisibility': 1,
            'attachmentIds': '',
            'programLinks': '',
            'publicEvent': 'true',
            'specialPassword': '',
            'confirmPassword': '',
            'action': 'edit',
        }

        data = urllib.urlencode(data)
        self.opener.open(url, data)

    def export_program(self, program):
        ''' Prepare a program on BP for local download'''

        url = 'http://130.111.228.122/create_allPrograms_export.html'
        data = {
            'mode': 'export',
            'programId': program.id,
            'primaryVideo': 'true',
            'dojo.preventCache': '1455286794488'
        }

        data = urllib.urlencode(data)
        self.opener.open(url, data)
        time.sleep(5)
        logging.info('Exported Program: ' + program.name)

        url = 'http://130.111.228.122/create_allPrograms_export.html'
        data = {
            'mode': 'refresh',
            'programId': program.id,
            'dojo.preventCache': '1455286794488'
        }

        data = urllib.urlencode(data)
        page = self._download_page(url, data)
        download_link = page.xpath("//a[contains(@href, '.mp4')]")
        logging.info('Program ' + program.name + ' ready to download.')

        return download_link[0].attrib['href']

    def download_media_file(self, program, dest):
        ''' Download a media file from BP to local file '''

        download_link = self.export_program(program)
        urllib.urlretrieve(download_link, filename=dest)




if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    test = BP()
    test.login()

    #test.download_channels()
    #test.download_folders()
    #test.download_library()
    #test.download_programs()
    #print test.download_live_events()
