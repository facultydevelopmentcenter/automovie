'''
Faculty Development Center
Robert Powell
Last-Updated 3/4/2014
'''

from bp2 import *
from os import listdir
import argparse, subprocess, pickle

def downloadBP():
    log = Reporting()
    control = Controller(log)
    control.setupInterface()
    control.setupLocals()
    control.createVideos()
    return control

def emailVideos(owner, videos, email):
    log = Reporting()
    emailSender(owner, videos, log, email)

def listDirectory(folder):
    videoNames= []
    allfiles = listdir(folder)
    for filename in allfiles:
        if filename[-4:] in ('.avi', '.wmv', '.mov', 'rmbv', '.flv', '.mpg', '.mp4', '.MTS'):
            videoNames.append(filename[0:-4] + '.mp4')
    return videoNames

def updateVideos(folder, owner, email = ''):
    videoList = listDirectory(folder)
    bp = downloadBP()
    updatedVideos = bp.updateVideos(owner, videoList)
    if email:
        emailVideos(owner, updatedVideos, email)

def batchHandbrake(folder, owner = False, upload = False):
    log = Reporting()
    if not (owner): owner = "FDC_Crew"
    magCopy = magicCopy(folder, owner, log, True)
    magCopy.findMovies()
    if upload:
        arbiter(folder, owner)

def watchDirs():
    subprocess.Popen(["python","watchDir.py"])

def Upload(folder, owner):
    log = Reporting()
    arbiter(folder, owner, False)

def saveMap(ownerHash, allPrograms):
    pickle.dump(ownerHash, open("owners.p","wb"))
    pickle.dump(allPrograms, open("programs.p","wb"))

def loadMap():
    oHash = pickle.load(open("owners.p", "rb"))
    allP = pickle.load(open("programs.p", "rb"))
    return (oHash, allP)

def findVideos(owner, rld, email = ''):
    oMap = {}
    if rld:
        bp = downloadBP()
        for channel in bp.channelHash:
            oMap[channel] = []
        for program in bp.allPrograms:
            if program.Channel in oMap:
                oMap[program.Channel].append(program)
        saveMap(oMap, bp.allPrograms)
    else:
        oMap = loadMap()[0]

    if owner in oMap:
        for program in oMap[owner]:
            print program.name
            print program.url
            print ''
    else: print "Entered channel does not exist, perhaps refresh?"
    if email:
        emailVideos(owner, oMap[owner], email)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'A faster way to deal with BurstPoint') 
    parser.add_argument('owner', type = str, help = 'Owner of the videos in the folder')
    parser.add_argument('-f', '--folder', type = str, help = 'Video source directory')
    parser.add_argument('-u', '--update', action = 'store_true', help = 'Update BP programs')
    parser.add_argument('-e', '--email', type = str, dest = 'email', help = 'Specify email parameter')
    parser.add_argument('-H', '--Handbrake', action = 'store_true', help = 'Handbrake all videos in source')
    parser.add_argument('-U', '--Upload', action = 'store_true', help = 'Upload files to BP')
    parser.add_argument('-W', '--auto', action = 'store_true', help = 'Start the autoMovie process')
    parser.add_argument('-F', '--find', action = 'store_true', help = 'List videos of a channel')
    parser.add_argument('-X', '--refresh', action = 'store_true', help = "Update the video cache information")
    args = parser.parse_args()
    if (args.Handbrake or args.Upload):
        if (args.Handbrake):
            batchHandbrake(args.folder, args.owner, args.Upload)
        if (args.Upload):
            Upload(args.folder, args.owner)
    elif (args.update):
            updateVideos(args.folder, args.owner, args.email)
    elif (args.find):
        findVideos(args.owner, args.refresh, args.email)

