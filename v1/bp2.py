from BeautifulSoup import *
from threading import Lock
from os import listdir, system, rename
import sys
import shutil
import time
import urllib
import urllib2
import cookielib
import json
import smtplib
import random
import pickle
import subprocess


class Files(object):
    """
    Class that handles copying files from some folder to vodpub.
    Also manages the settings file in vodpub for different video types.
    """
    #The settings file in vodpub
    global configFile
    configFile = "/Volumes/publishers/vodpub/settings.cfg"
    #Actual path to vodpub folder
    global videoDir
    videoDir = "/Volumes/publishers/vodpub"
    #Where to put the videos after processed.
    global doneDir
    doneDir = "../../Desktop/Done"
    #Where to convert videos like .MTS
    global convertDir
    convertDir = "../../Desktop/Converting"

    def __init__(self, source, name, log, encode=True):
        """Create the Files object"""
        #TODO Gotta delete this log thing
        self.log = log
        self.source = source
        self.name = name
        self.shortName = name.split("_")
        self.numVideos = 0
        self.mVideos = []
        self.failedVideos = []
        self.mountServer()
        self.encodeFlag = encode

    #Find all movies present in the source directory.
    def findMovies(self):
        tfiles = []
        files = listdir(self.source)
        for filename in files:
            tfiles.append(filename)

        #Filter out just the movies from the rest.
        #Also filter out the .mp4s from everything else because they are handled differently.
        for f in tfiles:
            if f[-4:] == '.mp4':
                cleanVideo = self.cleanName(f)
                print cleanVideo, "Add this to mVideos!"
                self.mVideos.append(cleanVideo)
                print 'Not handbraking this one - already an MP4'
            elif f[-4:] in ('.avi', '.wmv', '.mov', 'rmbv', '.flv', '.mpg'):
                cleanVideo = self.cleanName(f)
                if self.encodeFlag:
                    shutil.move(self.source + '/' + cleanVideo, convertDir)
                    cStat = self.handBrake(cleanVideo)
                    if cStat:
                        self.mVideos.append(cStat)
                else:
                    self.mVideos.append(cleanVideo)
                print "Added video file for processing"
            elif f[-4:] in ('.MTS'):
                cleanVideo = self.cleanName(f)
                shutil.move(self.source + '/' + cleanVideo, convertDir)
                cStat = self.handBrake(cleanVideo)
                if cStat:
                    self.mVideos.append(cStat)
        self.numVideos = len(self.mVideos)
        #In the case that no valid videos were detected
        #Call the error object and log that something probably went wrong.
        if self.numVideos == 0:
            self.log.takeLog('MagicCopy', "Number of detected videos is zero! Unsupported filetype in the directory " + self.source + ' for ' + self.name)
            self.log.sendError('It looks like an unsupported file was dropped into the box! Check the folder for ' + self.name)

    #Need to clean the filenames of all meta-characters that would break BP
    def cleanName(self, videoName):
        keepChars = "_."
        vName = videoName[0:-4]
        ext = videoName[-4:]
        newName = "".join(c for c in vName if c.isalpha() or c.isdigit()).rstrip()
        newName = newName + ext
        rename(self.source + '/' + videoName, self.source + '/' + newName)
        return newName

    #Copy all the found videos into the vodpub directory.
    #Take a log of every video that is handled.
    def copyFiles(self,videos):
        for video in videos:
            self.tlog(video)
            shutil.copy2(self.source + "/" + video, videoDir)
            print "Copied", video, "to the vodoub Folder"

    #Move the videos into the vodpub folder.
    #Note: Set the settings file to equal 0 then transfer the mp4s first.
    #Then continue onward and copy the other videos.
    def move(self):
        self.changeSettings("0")
        print self.mVideos
        self.copyFiles(self.mVideos)
        failed1 = self.watchDir(self.mVideos)
        renamedM = []
        self.copyFiles(renamedM)
        renamed = set(list(renamedM))
        self.cleanUp()
        return (self.mVideos, renamed)

    #Input a list of video filenames and append a random number onto the end.
    def rename(self, videos):
        ren = []
        for filename in videos:
            filenameNew = filename[0:-4] + str(random.randint(1,100)) + filename[-4:]
            rename(self.source+'/'+filename, self.source+'/'+filenameNew)
            filename = filenameNew
            ren.append(filename)
            print "Renamed a video to ",filename
        print "Well at least this entered this function!"
        return ren


    #After videos have been copied and uploaded to BP
    #Clean all handled videos out of the original source folder.
    #Note that this doesn't move files that had not been uploaded!
    def cleanUp(self):
        for video in self.mVideos:
            shutil.move(self.source + '/' + video, doneDir)
            self.log.takeLog('MagicMovie', 'Moving ' + video + ' to the done directory')


    #Change the settings folder in vodpub.
    #0 means don't encode so use it for the mp4s
    #1 means encode so use it for everything else
    #TODO Updated so it never uses bp encoder
    def changeSettings(self, value):
        cFile = open(configFile, 'r')
        line = cFile.readline()
        line = line[:10] + value
        cFile.close()
        wFile = open(configFile, 'w')
        wFile.write(line)
        wFile.close()

    #Just make a simple log file so if this breaks, or fails
    #we'll know what videos already got processed etc etc
    def tlog(self,videoName):
        self.log.takeLog('Magic Copy', self.name + ' ' + videoName + ' was put into vodpub')


    #Just watch the directory until all videos in the provided list
    #no longer appear in the directory.
    #Will also check to see if any .errs appear in the directory then remove those videos from the waitlist.
    def watchDir(self, source):
        delay = True
        bads = set()
        while delay:
            files = listdir(videoDir)
            time.sleep(30)
            if [i for i in source if i in files]:
                pBads = self.checkBads(files)
                if len(pBads) != 0:
                    bads |= pBads
                    for bad in pBads:
                        if bad in source:
                            source.remove(bad)
                delay = True
            else:
                delay = False

        return bads

    #Attempt to mount vodpub, doesn't matter if it fails as it won't halt the program
    #Worst case is that the server is already mounted, which is what we want.
    def mountServer(self):
        setupdir = "mkdir /Volumes/publishers"
        commandString = "mount_smbfs //facdev:'BPvodpwd28!'@130.111.228.122/publishers /Volumes/publishers"
        try:
            subprocess.check_call(setupdir, shell = True)
            subprocess.check_call(commandString, shell = True)
            print "Mounted vodpub...no more errors here"
        except:
            print "Vodpub was either already mounted or the internet was down...meh"
            pass

    def checkBads(self,files):
        fVideos = []
        for f in files:
            badName = f[0:-4] + '.err'
            if badName in files:
                fVideos.append(f)
        return set(fVideos)

    def handBrake(self, fileName):
        commandString = "/Applications/HandBrakeCLI -i '../../Desktop/Converting/{}' -o '../../Desktop/Converting/{}' -f mp4 -O -e x264 -b 700 -2 -T --vfr"
        outputFile = fileName[0:-4] + '.mp4'
        try:
            print commandString.format(fileName, outputFile)
            subprocess.check_call(commandString.format(fileName, outputFile), shell = True)
            print 'Well that was a little too snappy!'
        except:
            self.log.sendError('A file conversion failed on file ' + fileName)
            #0 means it died, handbrake thinks 0 executes fine?
            return 0
        shutil.move('../../Desktop/Converting' + '/' + outputFile, self.source)
        return outputFile

    def updateRemoteLog(self, updates):
        with open(self.source + '/videoList.txt' , 'a') as f:
            for video in updates:
                f.write(video.name + ": \n")
                f.write(video.url + ": \n")
        f.close()

class emailSender(object):
    '''
    Class that sends emails when video uploads are complete or have failed.
    Needs to be switched up a little!
    '''

    def __init__(self, receip, videos, log, email = ''):

        self.log = log
        sender = "fdc@umit.maine.edu"
        cc = "fdc@maine.edu"
        name = receip.split('_')
        emailDic = pickle.load(open('emails.dat','rb'))
        if receip in emailDic: receiver = emailDic[receip]
        else: receiver = receip.replace('_','.') + "@umit.maine.edu"
        if email:
            receiver = email

        message = "From %s\r\n" % sender
        message += "To: %s\r\n" % receiver
        message += "CC: %s\r\n" % cc
        message += "Subject: Streaming Links \n"
        message += "Hello %s, \n \n" %name[0]
        message += "Below are the links to your recently uploaded videos. \n\n"

        for video in videos:
            message += video.name + ": \n"
            message += video.url + "\n \n"
        message += "If you would like links to all your available videos just let us know!\n\n"
        message += "Please check to make sure the uploaded video/s is not cutoff at the end\n\n."
        message += "Thanks, \nFDC\n\n"
        message += "Faculty Development Center\n"
        message += "149 Memorial Union\n"
        message += "University of Maine, Orono, ME 04469\n"
        message += "207-581-1925\n"
        message += "http://www.umaine.edu/fdc"

        if len(videos) > 0:
            try:
               smtpObj = smtplib.SMTP('mail.maine.edu')
               smtpObj.sendmail(sender, receiver, message)
               self.log.takeLog('Email', 'Successfuly sent email to ' + receiver)
               print "Successfully sent email"
            except smtplib.SMTPException:
               print "Error: unable to send email"
               self.log.takeLog('Email', 'Could not send email to ' + receiver)
               self.log.sendError('Error attempting to send email to ' + receiver)
        else:
            print "HA! Didn't send a blank email this time! Woot woot woot!!!"


class Reporting(object):
    '''
    Class that handles writing to a logfile to keep track of everything
    that the program does -- EVERYTHING --
    '''

    #Since multiple threads are possible a semaphore is needed on the logfile.
    logFile = 'log.txt'
    logLocked = Lock()

    #Initialize the object with the logfile.
    def __init__(self):
        self.logFile = 'log.txt'

    #Write a message of the logfile stating the sender and message.
    def takeLog(self, sender, message):
        self.logLocked.acquire()
        with open(self.logFile , 'a') as f:
            f.write(sender + ': ' + message + '\n')
        f.close()
        self.logLocked.release()

    #When an error is encountered, send an email to FDC with some details.
    #In the case that the email itself can't be sent, write it to the logfile and
    #hope for the best...
    def sendError(self, errorMessage):
        sender = "fdc@umit.maine.edu"
        cc = "fdc@umit.maine.edu"
        receiver = sender
        message = "From %s\r\n" % sender
        message += "To: %s\r\n" % receiver
        message += "CC: %s\r\n" % cc
        message += "Subject: MovieMagic Error\n"
        if type(errorMessage) == type([]):
            vids = errorMessage[0]
            name = errorMessage[1]
            for part in vids:
                message += part + ' '
            message += "video/s did not encode properly, they/it may already exist on Burstpoint, or vodpub died.\n"
            message += "Check the video folder for, " + name +'.'
        else:
            message += errorMessage
        try:
           smtpObj = smtplib.SMTP('mail.maine.edu')
           smtpObj.sendmail(sender, receiver, message)
           self.takeLog('Email', 'Successfuly sent an error email ')
           print "Successfully sent email"
        except smtplib.SMTPException:
           print "Error: unable to send email"
           self.takeLog('Email', 'Error reporting email could not be sent... something really broke...')
        sys.exit()


class webDriver(object):
    '''
    Class that handles the interactions with burstpoint.
    Downloads the raw html associated with the videos, categories, and channels listed on BP.
    Also has a method to execute specific urls on the BP.
    '''

    #Init the object pretty much blank besides having a username and password.
    def __init__(self, log):
        self.log = log
        self.username = 'fdcit'
        self.password = 'PWfdcit#1'
        self.cj = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cj))
        self.rawPage = ''
        self.bpVideos = []
        self.bpFiles = []

    #Method to run a specified update-url to change category/channel of video.
    def updateVideo(self, Url):
        print 'Updated URL: ' + Url
        self.log.takeLog('Webdriver', 'Running updateURL: ' + Url)
        self.opener.open(Url)

    #Method to authenticate to BP
    #Doesn't currently check to see if authentication was successful -> pain in the butt.
    def loginBP(self):
        login_data = urllib.urlencode({'_spring_security_remember_me':'true',
                                       'j_username':self.username,
                                       'j_password':self.password})
        self.opener.open('http://130.111.228.122/j_spring_security_check',login_data)
        print "Logged into BP"

    #Method to download all the videos listed on the allPrograms page of BP.
    #Will later change to download the entire collection of BP.
    def downloadVideos(self):
        self.bpVideos = []
        page = self.opener.open('http://130.111.228.122/create_allPrograms.html?pStart=-1')
        self.rawPage = BeautifulSoup(page.read())
        urls = self.rawPage.findAll(id = re.compile('programResultsTableForm'))
        self.videos = []
        for form in urls:
            table = form.findAll('table')
            for t in table:
                row = t.findAll('tr')
                for r in row:
                    self.bpVideos.append(r)
        #The first and last entries returned from Beautiful Soup are crud.
        self.videos = self.bpVideos[1:-1]
        print "Downloaded Raw Videos"

    #Download programs from live captures and add it to self.videos
    # def downloadLiveCaptures(self):
    #     self.bpVideos = []
    #     page = self.opener.open('http://130.111.228.122/create_allPrograms.html?pStart=-1')
    #     self.rawPage = BeautifulSoup(page.read())
    #     urls = self.rawPage.findAll(id = re.compile('programResultsTableForm'))
    #     self.videos = []
    #     for form in urls:
    #         table = form.findAll('table')
    #         for t in table:
    #             row = t.findAll('tr')
    #             for r in row:
    #                 self.bpVideos.append(r)
    #     #The first and last entries returned from Beautiful Soup are crud.
    #     self.videos = self.bpVideos[1:-1]
    #     print "Downloaded Raw Videos"

    #Method to download ALL the files, not programs, from Burstpoint.
    #There should also be a method to save these to a file, and given that a certain time period
    #has not gone by, should access the file list from there.
    def downloadFiles(self):
        self.bpFiles = []
        page = self.opener.open('http://130.111.228.122/create_library.html?pNum=-1')
        self.rawPage = BeautifulSoup(page.read())
        urls = self.rawPage.findAll(id = re.compile('programResultsTableForm'))
        for form in urls:
            table = form.findAll('table')
            for t in table:
                row = t.findAll('tr')
                for r in row:
                    self.bpFiles.append(r)
        #The first and last entries returned from Beautiful Soup are crud.
        self.bpFiles = self.bpFiles[1:-1]
        print "Downloaded all the files from Burstpoint!"


    #Method to download the different categories and channels listed on BP.
    #Currently downloads ALL of them -> Can save time in future by storing these somewhere.
    def downloadCats(self):
        page = self.opener.open('http://130.111.228.122/create_allPrograms.html')
        self.rawPage = BeautifulSoup(page.read())
        self.catMap = self.rawPage.findAll({'div':True, 'script':True, 'class':'catTree'}, id = re.compile('catTree'))
        self.log.takeLog('webDriver', 'Successfully pulled raw html from BP')
        print "Downloaded Categories"


    #Given a BP file's resource ID, lookup information and return a list of program references.
    def findFilePrograms(self, resourceID):
        page = self.opener.open('http://130.111.228.122/create_library_edit.html?resourceId='+resourceID)
        rawPage = BeautifulSoup(page.read())
        programTable = rawPage.findAll(id = re.compile('resourceProgramsDiv'))
        refs = []
        for references in programTable:
            referenceList = references.findAll('a',href=True)
            if len(referenceList) != 0:
                for ref in referenceList:
                    refs.append(ref.text)
                return refs
            else:
                return ''


class Parser(object):
    '''
    Class that takes all the raw html from the webdriver and converts it to useful stuff.
    '''

    #Really an abstract class, it doesn't need to be init'd
    def __init__(self):
        spooty = "foo"

    #Take the html from BP and create a hashtable of channel ids to names.
    def getChannelMap(self, something):
        catMap = something
        catMap = str(catMap[0].text)
        catMap = catMap.split('[')
        #If it stops working again try changing it to catMap[6] or catMap[5]
        #For some reason they only change the location of a set of variables, derp...
        catList = catMap[7]
        catList = catList[:catList.rfind(']')]
        catList = catList[:-1]
        catList = catList.split('},')
        tdata = []
        channelMap = {}
        for cateItem in catList:
            tdata.append(json.loads(cateItem+'}'))
        for item in tdata:
            channelMap[item['name']] = item['id'][3:]
        print 'Created all Categories!'
        print channelMap
        return channelMap

    #I must've been retarded when I did this.
    #It returns all the parts needed to create a video object.
    def extractProgram(self, rawRow):
        #self.tRow = rawRow
        return (self.extractID(rawRow), self.extractURL(rawRow), self.extractName(rawRow), self.extractChannel(rawRow))

    def extractFile(self, rawRow):
        return (self.extractName(rawRow), '', '', '', self.extractFileID(rawRow))

    #Grab the video ID from the raw html
    def extractID(self, rRow):
        tRow = rRow.findAll({'a':True}, href = re.compile('create_allPrograms_programList.html'))
        return tRow[0]['href'][63:68]

    #Grab the actual email-able URL to the video.
    def extractURL(self, rRow):
        tRow = rRow.findAll({'a':True}, href = re.compile('/viewResource.html'))
        return tRow[0]['href']

    #Grab the name of the video/file.
    def extractName(self, rRow):
        tRow = rRow.findAll({'td':True}, limit = 2)
        tempArray = []
        for row in tRow:
            tempArray.append(row.extract())
        return tempArray[1].text

    #Grab what channel the video belongs to.
    def extractChannel(self, rRow):
        tRow = rRow.findAll({'td':True}, limit = 4)
        tempArray = []
        for row in tRow:
            tempArray.append(row.extract())
        return tempArray[1].text

    #Grab a BP file resourceId
    def extractFileID(self,rRow):
        a = rRow.findAll('a',href=True)
        rID = a[0]['href']
        rID = rID.split('=')
        return rID[1]


class Video(object):
    '''
    Class that works as abstraction for the BP videos.
    '''

    #Gets created with all the information from BP.
    def __init__(self, ID, Name, Desc, Url, Channel):
        self.id = ID
        self.name = unicode(Name)
        self.desc = unicode(Desc + ':'+ '   ')
        self.url = 'http://bp-manager.ume.maine.edu' + unicode(Url)
        self.Channel = unicode(Channel)
        self.updateURL = ''

    #Update the faculty channel associated with the video.
    def updateChannel(self, newChannel):
        self.Channel = newChannel

    #The URL that needs to be executed in order to upload the local
    #video's information to BP (Channel, name, category, etc).
    def getUpdateURL(self):
        self.updateURL = 'http://130.111.228.122/create_allPrograms_programList.html?preventCache=1380651888644&programType=vod&thumbnailId=2439&programId=' +self.id+ '&programName=' +urllib.quote_plus(self.name).replace('+', '%20')+ '&description=' +urllib.quote_plus(self.desc).replace('+','%20')+ '&programCategoryId=11&programChannelId=' +self.Channel+ '&themeName=MP4_Video_only&programURL=http%3A%2F%2Fbp-manager.ume.maine.edu%2F' +urllib.quote_plus(self.url[32:])+ '&programVisibility=1&attachmentIds=&programLinks=&publicEvent=true&specialPassword=&confirmPasswd=&embedSilverlight=&action=edit&pStart=0&pNum=20'
        return self.updateURL


class LibraryFile(object):
    '''
    Class that works as abstraction for the BP file library elements
    '''

    def __init__(self, Name, Folder, Owner, ProgramArray, resourceID):
        self.name = Name
        self.folder = Folder
        self.owner = Owner
        self.pArray = ProgramArray
        self.rID = resourceID


class Controller(object):
    '''
    Class that acts basically like a buffer between local copies of movies and those found on BP.
    Makes sure everything on BP matches what it should according to what this class is handed.
    '''

    #Initialize the object with stuff do to.
    def __init__(self, log):
        print "Controller Object has been Created"
        self.log = log
        self.bpInterface = webDriver(log)
        self.parse = Parser()
        self.allPrograms = []
        self.allFiles = {}
        self.channelHash = {}

    #Login to BP, download the categories/channels, and finally download the videos.
    def setupInterface(self):
        self.bpInterface.loginBP()
        self.bpInterface.downloadCats()
        self.bpInterface.downloadVideos()
        self.bpInterface.downloadFiles()

    #Get the raw category/channel information from BP pass it to the parser.
    #Get the final dictionary of the channel id -> name mappings.
    def setupLocals(self):
        tMap = self.bpInterface.catMap
        self.channelHash = self.parse.getChannelMap(tMap)

    #Filter through the raw videos from BP and pass them to the parser.
    #Use that to create video objects which are then stored in videoArray.
    def createVideos(self):
        t = []
        self.allPrograms = []
        for bpFile in self.bpInterface.bpFiles:
            tup = self.parse.extractFile(bpFile)
            self.allFiles[tup[0]] = (LibraryFile(tup[0],tup[1],tup[2],tup[3],tup[4]))
        print 'Created all file objects'
        for video in self.bpInterface.videos:
            tup = self.parse.extractProgram(video)
            self.allPrograms.append(Video(tup[0],tup[2],tup[2],tup[1],tup[3]))
        print 'Created all program objects'


    def assocPrograms(self, files):
        for bpFile in files:
            bpFile.pArray = self.bpInterface.findFilePrograms(bpFile.rID)


    #Update any videos in the provided list to the provided owner.
    #Return the which videos were succesfully updated.
    def updateVideos(self, owner, videoList):
        temp = []
        for bpVideo in self.allPrograms:
            for localVideo in videoList:
                if bpVideo.name == localVideo[0:-4]:
                    print "Working with, " , bpVideo.name
                    print self.channelHash
                    bpVideo.updateChannel(self.channelHash[owner])
                    updateUrl = bpVideo.getUpdateURL()
                    self.bpInterface.updateVideo(updateUrl)
                    self.log.takeLog('Controller', 'Updated video '+ bpVideo.name + ' to ' + owner)
                    temp.append(bpVideo)
        return temp

    def update(self, owner, Videos):
        retries = 5
        while retries > 0:
            time.sleep(10)
            self.setupInterface()
            self.setupLocals()
            self.videoArray = []
            self.createVideos()
            ul = self.updateVideos(owner, Videos)
            if len(Videos) == len(ul):
                return (True,ul)
            retries -= 1
        return (False,ul)


    #Return that last set of downloaded videos from BP.
    def getVids(self):
        return self.videoArray


class arbiter(object):
    '''
    Toplevel class that gets everything rolling.
    Requires a source directory of videos and an owner of those videos.
    Can be launched from a GUI, CommandLine, or other Program.
    '''

    def __init__(self,source, name, encode = True):
        self.log = Reporting()
        if source == '' or name == '':
            self.log.takeLog('Arbiter', 'Program was launched without a name or source dir!')
            self.log.sendError('The program was launched without a Name or Source directory')

        self.preStart(source,name, encode)

        print self.mag.mVideos
        returned = self.mag.move()
        self.videoNames = returned[0]
        self.failed = returned[1]

        #Send an error email if some videos didn't encode correctly from vodpub
        if len(self.failed) != 0:
            self.log.sendError([self.failed,name])

        print self.videoNames

        self.updatedStatus = self.controller.update(name, self.videoNames+list(self.failed))
        print self.updatedStatus[0],self.updatedStatus[1]

        self.videoArray = self.controller.getVids()
        print self.videoArray
        if len(self.videoArray):
            self.log.takeLog('Arbiter', "Error: Going to send email with no videos!")
            self.log.sendError('I broke again! But this time Im not going to send an email to the instructor!')
        else:
            self.newEmail = emailSender(name, self.updatedStatus[1], self.log)
            #self.mag.updateRemoteLog(self.updatedStatus[1])

        system('say "Uploaded new stuff"')

    def preStart(self, source, name, encode):
        self.controller = Controller(self.log)
        self.controller.setupInterface()
        self.controller.setupLocals()
        self.controller.createVideos()

        self.mag = magicCopy(source, name, self.log, encode)
        self.mag.findMovies()
        print self.mag.mVideos

        self.duplicates = self.detectDups((self.mag.mVideos), self.controller.allFiles)

        if self.duplicates[0]:
            renamed = self.mag.rename(self.duplicates[0])
            self.mag.mVideos = self.mag.mVideos + renamed
            self.controller.assocPrograms(self.duplicates[1])
            self.dupEmail(self.duplicates[0])
            sys.exit(0)


    def dupEmail(self, duplicateFiles):
        print "Found duplicates, ", duplicateFiles
        print "So I stopped running this Thread."


    def detectDups(self, localFiles, bpFiles):
        print "At least I got called!"
        dups = []
        bpDups = []
        for vidTuple in localFiles:
            if len(localFiles) == 1:
                cName = vidTuple[0:-4] + '.mp4'
                for bFile in bpFiles:
                    if cName == bFile:
                        print 'Found a match!'
                        dups.append(vidTuple)
                        bpDups.append(bpFiles[cName])
                        localFiles = ''
            else:
                for locFile in vidTuple:
                    cName = locFile[0:-4] + '.mp4'
                    for bFile in bpFiles:
                        if cName == bFile:
                            print 'Found a match!'
                            dups.append(locFile)
                            bpDups.append(bpFiles[cName])
                            vidTuple.remove(locFile)

        return (dups,bpDups)
