"""
@Author Robert Powell
@Class MagicCopy

Class to handle local file management for videos
"""

from os import listdir, rename
import shutil
import time
import subprocess
import logging


class Files(object):
    """
    Class that handles copying files from some folder to vodpub.
    """
    #The settings file in vodpub
    global configFile
    configFile = "/Volumes/publishers/vodpub/settings.cfg"
    #Actual path to vodpub folder
    global videoDir
    videoDir = "/Volumes/publishers/vodpub"
    #Where to put the videos after processed.
    global doneDir
    doneDir = "../../Desktop/Done"
    #Where to convert videos like .MTS
    global convertDir
    convertDir = "../../Desktop/Converting"

    def __init__(self, source, name, encode=True):
        """Create the Files object"""

        self.source = source
        self.name = name
        self.short_name = name.split("_")
        self.num_videos = 0
        self.m_videos = []
        self.failed_videos = []
        self.mount_server()
        self.encode_flag = encode

    def find_movies(self):
        ''' Find all movies present in the source directory.'''
        tfiles = []
        files = listdir(self.source)
        for filename in files:
            tfiles.append(filename)


        for f in tfiles:
            if f[-4:] == '.mp4':
                clean_video = self.clean_name(f)
                #print cleanVideo, "Add this to mVideos!"
                self.m_videos.append(clean_video)
                logging.info('Detected .mp4 ' + clean_video)
                #print 'Not handbraking this one - already an MP4'
            elif f[-4:] in ('.avi', '.wmv', '.mov', 'rmbv', '.flv', '.mpg'):
                clean_video = self.clean_name(f)
                logging.info('Detected video file ' + clean_video)
                if self.encode_flag:
                    shutil.move(self.source + '/' + clean_video, convertDir)
                    clean_stat = self.handbrake(clean_video)
                    if clean_stat:
                        self.m_videos.append(clean_stat)
                else:
                    self.m_videos.append(clean_video)
            elif f[-4:] in ('.MTS'):
                clean_video = self.clean_name(f)
                logging.info('Detected .MTS file ' + clean_video)
                shutil.move(self.source + '/' + clean_video, convertDir)
                clean_stat = self.handbrake(clean_video)
                if clean_stat:
                    self.m_videos.append(clean_stat)
        self.numVideos = len(self.m_videos)

        if self.numVideos == 0:
            logging.error('Triggered upload, but no videos found. \
                    Unsupported file may have been detected')

    def clean_name(self, video_name):
        ''' Strip non alpha-numeric characters from file name '''
        v_name = video_name[0:-4]
        ext = video_name[-4:]
        new_name = "".join(c for c in v_name if c.isalpha() or c.isdigit()).rstrip()
        new_name = new_name + ext
        rename(self.source + '/' + video_name, self.source + '/' + new_name)
        return new_name

    def copyFiles(self, videos):
        ''' Copy videos into vodpub '''

        for video in videos:
            shutil.copy2(self.source + "/" + video, videoDir)
            logging.info('Copied ' + video + ' to vodpub')

    def move(self):
        ''' Transfer the videos to BP '''

        self.change_settings("0")
        self.copyFiles(self.m_videos)
        failed = self.watch_dir(self.m_videos)

        for video in failed:
            logging.error('Failed to upload ' + video)

        renamed_m = []
        self.copyFiles(renamed_m)
        renamed = set(list(renamed_m))
        self.clean_up()
        return (self.m_videos, renamed)

    def clean_up(self):
        ''' Move all uploaded videos to DONE dir '''

        for video in self.m_videos:
            shutil.move(self.source + '/' + video, doneDir)
            logging.info('Cleaning... ' + video)

    def change_settings(self, value):
        ''' Change encode setting in vodpub '''

        config = open(configFile, 'r')
        line = config.readline()
        line = line[:10] + value
        config.close()
        w_file = open(configFile, 'w')
        w_file.write(line)
        w_file.close()

    def watch_dir(self, source):
        ''' Watch a directory until all videos are removed '''

        delay = True
        bads = set()
        while delay:
            time.sleep(30)
            files = listdir(videoDir)
            if [i for i in source if i in files]:
                pot_bads = self.check_bads(files)
                if len(pot_bads) != 0:
                    bads |= pot_bads
                    for bad in pot_bads:
                        if bad in source:
                            source.remove(bad)
                delay = True
            else:
                delay = False

        return bads

    def mount_server(self):
        ''' Attempt to mount vodpub remote folder '''

        setupdir = "mkdir /Volumes/publishers"
        command_string = "mount_smbfs //facdev:'BPvodpwd28!'@130.111.228.122/publishers /Volumes/publishers"
        try:
            subprocess.check_call(setupdir, shell=True)
            subprocess.check_call(command_string, shell=True)
            logging.info('Mounted remote vodpub drive')
        except:
            logging.info('Vodpub could not be re-mounted OK')

    def check_bads(self, files):
        ''' Handle .err files from vodpub '''

        found_videos = []
        for f in files:
            bad_name = f[0:-4] + '.err'
            if bad_name in files:
                found_videos.append(f)
        return set(found_videos)

    def handbrake(self, fileName):
        ''' Encode videos using handbrake '''

        command_string = "/Applications/HandBrakeCLI -i '../../Desktop/Converting/{}' -o '../../Desktop/Converting/{}' -f mp4 -O -e x264 -b 700 -2 -T --vfr"
        output_file = fileName[0:-4] + '.mp4'
        try:
            print command_string.format(fileName, output_file)
            subprocess.check_call(command_string.format(fileName, output_file),\
                    shell=True)
            logging.info('Handbraked... ' + output_file)
        except:
            logging.error('Handbraked fail conversion... ' + fileName)
            return 0

        shutil.move('../../Desktop/Converting' + '/' + output_file, self.source)
        return output_file
