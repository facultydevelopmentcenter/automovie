#!/usr/bin/python
import os, time
from os import system
from bp2 import *
from threading import Thread


class Monitor:
    CACHDIR = "~/Dropbox/.dropbox.cache/"
    def __init__(self, dirWatch, name):
        self.d = dirWatch
        self.name = name

    def watch(self):
        before = dict([(f, None) for f in os.listdir (self.d)])

        while 1:
            time.sleep(5)
            after = dict ([(f, None) for f in os.listdir (self.d)])
            added = [f for f in after if not f in before]
            removed = [f for f in before if not f in after]
            if (added):
                print "Added", " ".join(added)
                if self.watchCache():
                    temp = arbiter(self.d,self.name)
                
            before = after

    #Poll the Dropbox cache every 5 seconds to see if it's still increasing.
    def watchCache(self):
        while(1):
            sizeBefore = self.calcSize(self.CACHDIR)
            time.sleep(10)
            sizeAfter = self.calcSize(self.CACHDIR)

            if sizeBefore == sizeAfter:
                return True
            
    #Calculate the size of passed direcory and return it.
    def calcSize(self, directory):
        size = 0
        for dirpath, dirnames, filenames in os.walk(directory):
            for f in filenames:
                fp = os.path.join(dirpath,f)
                size += os.path.getsize(fp)
        return size
        
dirDic = {"../Irv.K/": "Irv_Kornfield",
          "../Jen Tyne/": "Jennifer_Tyne",
          "../Video Folder (Jim Artesani)/": "James_Artesani",
          "../Video Folder (Samuel Hanes)/": "Samuel_Hanes",
          "../Videos Carlton Brown/": "Carlton_Brown",
          "../Warren's videos/": "Warren_Riess",
          "../Robert_Powell/": "Robert_Powell",
          "../Mac_Gray/": "Mac_Gray",
          "../Paul_Holman/":"Paul_Holman",
          "../Jane_Smith/":"Jane_Smith",
          "../Duane's Videos/":"Duane_Shimmel",
          "../Steve_Shaler/":"Steve_Shaler",
          "../Al B. Videos":"Alfred_Bushway",
          "../Scott Dunning":"Scott_Dunning",
          "../Food Law Faculty Development":"Alfred_Bushway",
          "../FDC_Vids":"FDC_Crew",
          "../../Google Drive/Video Streaming/Nilda Cravens":"Nilda_Cravens",
          "../../Google Drive/Video Streaming/Elin Mackinnon Videos":"Elin_Mackinnon",
          "../../Google Drive/Video Streaming/Shelton Waldrep Videos":"Shelton_Waldrep",
          "../../Google Drive/Streaming Folders/Paul Villeneuve Videos":"Paul_Villeneuve",
          "../../Google Drive/Streaming Folders/Rod Bushway":"Rod_Bushway",
          "../../Google Drive/Streaming Folders/Raymond Hintz Video Stream":"Raymond_Hintz",
          "../../Google Drive/Video Streaming/Nicolle Littrell Videos":"Nicolle_Littrell",
          "../../Google Drive/Video Streaming/Jessica Brophy Videos":"Jessica_Brophy",
          "../../Google Drive/Video Streaming/Tandy Del Vecchio Videos":"Tandy_Del",
          }
          #"../../Desktop/Hannah":"Hannah_Todd",
          #"../../Desktop/Janet":"Janet_Spector",
          #"../../Desktop/Joan":"Joan_Perkins",
          #"../../Desktop/Julie":"Julie_Riley",
          #"../../Desktop/Mary":"Mary_Logue",
          #"../../Desktop/Gilson":"Stephen_Gilson",
          #"../../Desktop/Nancy":"Nancy_Fishwick",
          #"../../Desktop/Ellis":"Kathleen_Ellis",



          
watchers = []
threads = []
system('Say Derp derp cow')
for d in dirDic:
    watchers.append(Monitor(d, dirDic[d]))
    threads.append(Thread(target = watchers[-1].watch))
    threads[-1].start()

        
