from BeautifulSoup import *
import sys
import urllib
import urllib2
import cookielib
import json
import logging

class BP(object):
    ''' BP API implementation '''

    def __init__(self):
        self.username = 'fdcit'
        self.password = 'PWfdcit#1'
        self.cookie = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookie))
        self.raw_page = ''
        self.bp_videos = []
        self.bp_files = []
        self.videos = []

    def update_video(self, url):
        ''' Open the passed url to update a BP program '''

        logging.info('Executing update URL: ' + url)
        self.opener.open(url)

    def login(self):
        ''' Log into BP and store auth cookie '''

        login_data = urllib.urlencode({'_spring_security_remember_me':'true',
                                       'j_username':self.username,
                                       'j_password':self.password})
        self.opener.open('http://130.111.228.122/j_spring_security_check',\
                login_data)
        logging.info('Logged into BP')

    def download_videos(self):
        ''' Download all programs listed on BP '''

        page = self.opener.open('http://130.111.228.122/create_allPrograms.html?pStart=-1')
        raw_page = BeautifulSoup(page.read())
        urls = raw_page.findAll(id=re.compile('programResultsTableForm'))
        for form in urls:
            table = form.findAll('table')
            for t in table:
                row = t.findAll('tr')
                for r in row:
                    self.bp_videos.append(r)
        #The first and last entries returned from Beautiful Soup are crud.
        self.videos = self.bp_videos[1:-1]
        logging.info('Downloaded BP programs')

    def download_files(self):
        ''' Download BP library files '''

        self.bp_files = []
        page = self.opener.open('http://130.111.228.122/create_library.html?pNum=-1')
        raw_page = BeautifulSoup(page.read())
        urls = raw_page.findAll(id=re.compile('programResultsTableForm'))
        for form in urls:
            table = form.findAll('table')
            for t in table:
                row = t.findAll('tr')
                for r in row:
                    self.bp_files.append(r)
        #The first and last entries returned from Beautiful Soup are crud.
        self.bpFiles = self.bpFiles[1:-1]
        logging.info('Downloaded BP library')


    def download_cats(self):
        ''' Download BP channels and categories'''

        page = self.opener.open('http://130.111.228.122/create_allPrograms.html')
        raw_page = BeautifulSoup(page.read())
        self.cat_map = raw_page.findAll({'div':True, 'script':True, \
                'class':'catTree'}, id=re.compile('catTree'))
        logging.info('Downloaded BP channels')

class Parser(object):
    ''' Interpret html stored in the BP class '''

    def __init__(self):
        pass

    #Take the html from BP and create a hashtable of channel ids to names.
    def getChannelMap(self, something):
        catMap = something
        catMap = str(catMap[0].text)
        catMap = catMap.split('[')
        #If it stops working again try changing it to catMap[6] or catMap[5]
        #For some reason they only change the location of a set of variables, derp...
        catList = catMap[7]
        catList = catList[:catList.rfind(']')]
        catList = catList[:-1]
        catList = catList.split('},')
        tdata = []
        channelMap = {}
        for cateItem in catList:
            tdata.append(json.loads(cateItem+'}'))
        for item in tdata:
            channelMap[item['name']] = item['id'][3:]
        print 'Created all Categories!'
        print channelMap
        return channelMap

    #I must've been retarded when I did this.
    #It returns all the parts needed to create a video object.
    def extractProgram(self, rawRow):
        #self.tRow = rawRow
        return (self.extractID(rawRow), self.extractURL(rawRow), self.extractName(rawRow), self.extractChannel(rawRow))

    def extractFile(self, rawRow):
        return (self.extractName(rawRow), '', '', '', self.extractFileID(rawRow))

    #Grab the video ID from the raw html
    def extractID(self, rRow):
        tRow = rRow.findAll({'a':True}, href = re.compile('create_allPrograms_programList.html'))
        return tRow[0]['href'][63:68]

    #Grab the actual email-able URL to the video.
    def extractURL(self, rRow):
        tRow = rRow.findAll({'a':True}, href = re.compile('/viewResource.html'))
        return tRow[0]['href']

    #Grab the name of the video/file.
    def extractName(self, rRow):
        tRow = rRow.findAll({'td':True}, limit = 2)
        tempArray = []
        for row in tRow:
            tempArray.append(row.extract())
        return tempArray[1].text

    #Grab what channel the video belongs to.
    def extractChannel(self, rRow):
        tRow = rRow.findAll({'td':True}, limit = 4)
        tempArray = []
        for row in tRow:
            tempArray.append(row.extract())
        return tempArray[1].text

    #Grab a BP file resourceId
    def extractFileID(self,rRow):
        a = rRow.findAll('a',href=True)
        rID = a[0]['href']
        rID = rID.split('=')
        return rID[1]


class Video(object):
    '''
    Class that works as abstraction for the BP videos.
    '''

    #Gets created with all the information from BP.
    def __init__(self, ID, Name, Desc, Url, Channel):
        self.id = ID
        self.name = unicode(Name)
        self.desc = unicode(Desc + ':'+ '   ')
        self.url = 'http://bp-manager.ume.maine.edu' + unicode(Url)
        self.Channel = unicode(Channel)
        self.updateURL = ''

    #Update the faculty channel associated with the video.
    def updateChannel(self, newChannel):
        self.Channel = newChannel

    #The URL that needs to be executed in order to upload the local
    #video's information to BP (Channel, name, category, etc).
    def getUpdateURL(self):
        self.updateURL = 'http://130.111.228.122/create_allPrograms_programList.html?preventCache=1380651888644&programType=vod&thumbnailId=2439&programId=' +self.id+ '&programName=' +urllib.quote_plus(self.name).replace('+', '%20')+ '&description=' +urllib.quote_plus(self.desc).replace('+','%20')+ '&programCategoryId=11&programChannelId=' +self.Channel+ '&themeName=MP4_Video_only&programURL=http%3A%2F%2Fbp-manager.ume.maine.edu%2F' +urllib.quote_plus(self.url[32:])+ '&programVisibility=1&attachmentIds=&programLinks=&publicEvent=true&specialPassword=&confirmPasswd=&embedSilverlight=&action=edit&pStart=0&pNum=20'
        return self.updateURL


class LibraryFile(object):
    '''
    Class that works as abstraction for the BP file library elements
    '''

    def __init__(self, Name, Folder, Owner, ProgramArray, resourceID):
        self.name = Name
        self.folder = Folder
        self.owner = Owner
        self.pArray = ProgramArray
        self.rID = resourceID
