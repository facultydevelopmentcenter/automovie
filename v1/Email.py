'''
@Author Robert Powell
@Class Email

Send emails to FDC/Faculty with streaming links and updates
'''
import smtplib
import logging

class Emailer(object):
    '''
    Class that sends emails when video uploads are complete or have failed.
    '''

    def __init__(self, receip, videos, log, email =''):

        self.log = log
        sender = "fdc@umit.maine.edu"
        cc = "fdc@maine.edu"
        name = receip.split('_')
        emailDic = pickle.load(open('emails.dat','rb'))
        if receip in emailDic: receiver = emailDic[receip]
        else: receiver = receip.replace('_','.') + "@umit.maine.edu"
        if email:
            receiver = email

        message = "From %s\r\n" % sender
        message += "To: %s\r\n" % receiver
        message += "CC: %s\r\n" % cc
        message += "Subject: Streaming Links \n"
        message += "Hello %s, \n \n" %name[0]
        message += "Below are the links to your recently uploaded videos. \n\n"

        for video in videos:
            message += video.name + ": \n"
            message += video.url + "\n \n"

        message += "If you would like links to all your available videos just let us know!\n\n"
        message += "Please check to make sure the uploaded video/s is not cutoff at the end\n\n."
        message += "Thanks, \nFDC\n\n"
        message += "Faculty Development Center\n"
        message += "149 Memorial Union\n"
        message += "University of Maine, Orono, ME 04469\n"
        message += "207-581-1925\n"
        message += "http://www.umaine.edu/fdc"

        if len(videos) > 0:
            try:
                smtpObj = smtplib.SMTP('mail.maine.edu')
                smtpObj.sendmail(sender, receiver, message)
                logging.info('Sent video links to ' + receiver)
            except smtplib.SMTPException:
                logging.error('Could not send email to' + receiver)
        else:
            logging.error('Attempted to send blank email to' + receiver)

