from os import listdir, system, rename
from BeautifulSoup import *
import shutil
import time
import subprocess
import urllib
import urllib2
import cookielib
import json

class FileHandler(object):
    """Class to handle local file management and detection"""

    #The settings file in vodpub
    global CONFIG_FILE
    CONFIG_FILE = "/Volumes/publishers/vodpub/settings.cfg"
    #Actual path to vodpub folder
    global VIDEO_DIR
    VIDEO_DIR = "/Volumes/publishers/vodpub"
    #Where to put the videos after processed.
    global DONE_DIR
    DONE_DIR = "../../Desktop/Done"
    #Where to convert videos like .MTS
    global CONVERT_DIR
    CONVERT_DIR = "../../Desktop/Converting"

    def __init__(self, source, name, log, encode=True):
        self.log = log
        self.source = source
        self.name = name
        self.short_name = name.split("_")
        self.number_of_videos = 0
        self.mp4_videos = []
        self.failed_videos = []
        self.mount_server()
        self.encode = encode

    def find_movies(self):
        """Poll the set directory looking for video files"""
        clean_extensions = ['.avi', '.wmv', '.mov', 'rmbv', '.flv', '.mpg']
        dirty_extensions = ['.MTS']
        tfiles = []
        files = listdir(self.source)

        for filename in files:
            tfiles.append(filename)

        for filename in tfiles:
            if filename[-4:] == '.mp4':
                clean_video = self.clean_name(filename)
                print clean_video, "Add this to mVideos!"
                self.mp4_videos.append(clean_video)
                print 'Not handbraking this one - already an MP4'
            elif filename[-4:] in clean_extensions:
                clean_video = self.clean_name(filename)
                if self.encode:
                    shutil.move(self.source + '/' + clean_video, CONVERT_DIR)
                    clean_status = self.hand_brake(clean_video)
                    if clean_status:
                        self.mp4_videos.append(clean_status)
                else:
                    self.mp4_videos.append(clean_video)
                print "Added video file for processing"
            elif filename[-4:] in dirty_extensions:
                clean_video = self.clean_name(filename)
                shutil.move(self.source + '/' + clean_video, CONVERT_DIR)
                clean_status = self.hand_brake(clean_video)
                if clean_status:
                    self.mp4_videos.append(clean_status)
        self.number_of_videos = len(self.mp4_videos)

        if self.number_of_videos == 0:
            self.log.takeLog('MagicCopy', \
             'Number of detected videos is zero! \
               Unsupported filetype in the directory ' + self.source + \
               ' for ' + self.name)

            self.log.sendError('It looks like an unsupported file was \
                dropped into the box! Check the folder for ' + self.name)

    def clean_name(self, video_name):
        """Clean filenames for BP"""
        name = video_name[0:-4]
        ext = video_name[-4:]
        new = "".join(c for c in name if c.isalpha() or c.isdigit()).rstrip()
        new += ext
        rename(self.source + '/' + video_name, self.source + '/' + new)
        return new

    def copy_files(self, videos):
        """Copy referenced videos to vodpub folder"""
        for video in videos:
            self.take_log(video)
            shutil.copy2(self.source + "/" + video, VIDEO_DIR)
            print "Copied", video, "to the vodoub Folder"

    def move(self):
        """Prep vodpub folder and move held videos"""
        self.change_settings("0")
        self.copy_files(self.mp4_videos)
        self.watch_dir(self.mp4_videos)
        renamed = []
        self.copy_files(renamed)
        renamed = set(list(renamed))
        self.clean_up()
        return (self.mp4_videos, renamed)

    def rename(self, videos):
        """Append a _duplicate to the names of passed video files"""
        ren = []
        for filename in videos:
            new_filename = filename[0:-4] + '_duplicate' + filename[-4:]
            rename(self.source+'/'+filename, self.source+'/'+new_filename)
            filename = new_filename
            ren.append(filename)
        return ren

    def clean_up(self):
        """Moved successfully processed videos to the DONE directory"""
        for video in self.mp4_videos:
            shutil.move(self.source + '/' + video, DONE_DIR)
            self.log.take_log('MagicMovie', 'Moving ' \
                + video + ' to the done directory')


    #Isn't used anymore
    def change_settings(self, value):
        """Modify BP config file to set encoding on or off"""
        config = open(CONFIG_FILE, 'r')
        line = config.readline()
        line = line[:10] + value
        config.close()
        write_config = open(CONFIG_FILE, 'w')
        write_config.write(line)
        write_config.close()

    #GET RID OF THIS NONESENSE
    def take_log(self, video_name):
        """Take log"""
        self.log.takeLog('Magic Copy', \
            self.name + ' ' + video_name + ' was put into vodpub')

    def watch_dir(self, source):
        """Poll a directory until all processing files complete"""
        delay = True
        bads = set()
        while delay:
            files = listdir(VIDEO_DIR)
            time.sleep(30)
            if [i for i in source if i in files]:
                processed_bads = self.check_bads(files)
                if len(processed_bads) != 0:
                    bads |= processed_bads
                    for bad in processed_bads:
                        if bad in source:
                            source.remove(bad)
                delay = True
            else:
                delay = False

        return bads

    def mount_server(self):
        """Attempt to mount vodpub"""
        setup_dir = "mkdir /Volumes/publishers"
        command_string = "mount_smbfs //facdev:'BPvodpwd28!'@130.111.228.122/ publishers /Volumes/publishers"
        try:
            subprocess.check_call(setup_dir, shell=True)
            subprocess.check_call(command_string, shell=True)
            print "Mounted vodpub...no more errors here"
        except:
            print "Failed to remount vodpub, could be ok"

    def check_bads(self, files):
        """Look for error'ed files in referenced list"""
        failed_videos = []
        for f in files:
            bad_name = f[0:-4] + '.err'
            if bad_name in files:
                failed_videos.append(f)
        return set(failed_videos)

    def hand_brake(self, file_name):
        """Send hanbrake the encode command on the referenced filename"""
        commandString = "/Applications/HandBrakeCLI -i '../../Desktop/Converting/{}' -o '../../Desktop/Converting/{}' -f mp4 -O -e x264 -b 700 -2 -T --vfr"
        output_file = file_name[0:-4] + '.mp4'
        try:
            print commandString.format(file_name, output_file)
            subprocess.check_call(commandString.format(file_name, output_file), shell=True)
            print 'Well that was a little too snappy!'
        except:
            self.log.sendError('A file conversion failed on file ' + file_name)
            #0 means it died, handbrake thinks 0 executes fine?
            return 0
        shutil.move('../../Desktop/Converting' + '/' + output_file, self.source)
        return output_file

    def update_remote_log(self, updates):
        """Update the remote text file with list of current videos"""
        with open(self.source + '/videoList.txt', 'a') as remote_log:
            for video in updates:
                remote_log.write(video.name + ": \n")
                remote_log.write(video.url + ": \n")
        remote_log.close()


class WebHandler(object):
    """Class for handling the http stuff"""
    def __init__(self, log):
        self.log = log
        self.username = 'fdcit'
        self.password = 'PWfdcit#1'
        self.cookie_jar = cookielib.CookieJar()
        self.opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookie_jar))
        self.raw_page = ''
        self.bp_videos = []
        self.bp_files = []
        self.videos = []
        self.category_map = ""

    def update_video(self, url):
        """Execute the BP program update link"""
        print 'Updated URL: ' + url
        self.log.takeLog('Webdriver', 'Running updateURL: ' + url)
        self.opener.open(url)

    def login_bp(self):
        """Authenticate to BP"""
        login_data = urllib.urlencode({'_spring_security_remember_me':'true',
                                       'j_username':self.username,
                                       'j_password':self.password})
        self.opener.open('http://130.111.228.122/j_spring_security_check', \
            login_data)
        print "Logged into BP"

    def download_videos(self):
        """Download the HTML listing of BP programs"""
        self.bp_videos = []
        page = self.opener.open('http://130.111.228.122/create_allPrograms.html?pStart=-1')

        self.raw_page = BeautifulSoup(page.read())
        urls = self.raw_page.findAll(id=re.compile('programResultsTableForm'))
        for form in urls:
            tables = form.findAll('table')
            for table in tables:
                rows = table.findAll('tr')
                for row in rows:
                    self.bp_videos.append(row)
        #The first and last entries returned from Beautiful Soup are crud.
        self.videos = self.bp_videos[1:-1]
        print "Downloaded Raw Videos"

    def download_files(self):
        """Download the HTML listing of BP files"""
        self.bp_files = []
        page = self.opener.open('http://130.111.228.122/create_library.html?pNum=-1')
        self.raw_page = BeautifulSoup(page.read())
        urls = self.raw_page.findAll(id=re.compile('programResultsTableForm'))
        for form in urls:
            tables = form.findAll('table')
            for table in tables:
                rows = table.findAll('tr')
                for row in rows:
                    self.bp_files.append(row)
        #The first and last entries returned from Beautiful Soup are crud.
        self.bp_files = self.bp_files[1:-1]
        print "Downloaded all the files from Burstpoint!"

    def download_categories(self):
        """Download the HTML categories from BP"""
        page = self.opener.open('http://130.111.228.122/create_allPrograms.html')
        self.raw_page = BeautifulSoup(page.read())
        self.category_map = self.raw_page.findAll({ \
            'div':True, 'script':True, 'class':'catTree'}, \
             id=re.compile('catTree'))

        self.log.takeLog('webDriver', 'Successfully pulled raw html from BP')
        print "Downloaded Categories"

    def find_linked_programs(self, resource_id):
        """Given a BP file, return HTML of linked BP programs"""
        page = self.opener.open('http://130.111.228.122/create_library_edit.html?resourceId='+resource_id)
        raw_page = BeautifulSoup(page.read())
        programs = raw_page.findAll(id=re.compile('resourceProgramsDiv'))
        refs = []
        for references in programs:
            reference_list = references.findAll('a', href=True)
            if len(reference_list) != 0:
                for ref in reference_list:
                    refs.append(ref.text)
                return refs
            else:
                return ''


class Parser(object):
    """Parse html data to be used"""
    def __init__(self):
        pass

    def get_channel_map(self, something):
        """Create Hashtable of BP html for Channels"""
        category_html = something
        category_html = str(category_html[0].text)
        category_html = category_html.split('[')

        #Use either cat_html[7] or cat_html[6]
        category_list = category_html[7]
        category_list = category_list[:category_list.rfind(']')]
        category_list = category_list[:-1]
        category_list = category_list.split('},')

        temporary = []
        channel_map = {}

        for category_item in category_list:
            temporary.append(json.loads(category_item+'}'))
        for item in temporary:
            channel_map[item['name']] = item['id'][3:]
        print 'Created all Categories!'
        print channel_map
        return channel_map

    def extract_program(self, raw_row):
        """Compile pieces needed for BP video"""
        return (self.extract_id(raw_row), \
            self.extract_url(raw_row), \
            self.extract_name(raw_row), \
            self.extract_channel(raw_row))

    def extract_file(self, raw_row):
        """Grab associated filename"""
        return (self.extract_name(raw_row), \
         '', \
         '', \
         '', \
         self.extract_file_id(raw_row))

    def extract_id(self, raw_row):
        """Grab the video ID from the raw html"""
        temporary = raw_row.findAll({'a':True}, \
            href=re.compile('create_allPrograms_programList.html'))
        return temporary[0]['href'][63:68]

    def extract_url(self, raw_row):
        """Grab the actual email-able URL to the video."""
        temporary = raw_row.findAll({'a':True}, \
            href = re.compile('/viewResource.html'))
        return temporary[0]['href']

    def extract_name(self, raw_row):
        """Grab the name of the video/file."""
        temporary = raw_row.findAll({'td':True}, limit=2)
        temp_array = []
        for row in temporary:
            temp_array.append(row.extract())
        return temp_array[1].text

    def extract_channel(self, raw_row):
        """Grab what channel the video belongs to."""
        temporary = raw_row.findAll({'td':True}, limit=4)
        temp_array = []
        for row in temporary:
            temp_array.append(row.extract())
        return temp_array[1].text

    def extract_file_id(self, raw_row):
        """Grab a BP file resourceId"""
        link = raw_row.findAll('a', href=True)
        resource_id = link[0]['href']
        resource_id = resource_id.split('=')
        return resource_id[1]


class Video(object):
    """Data Object for BP programs"""

    #Gets created with all the information from BP.
    def __init__(self, ID, Name, Desc, Url, Channel):
        self.id = ID
        self.name = unicode(Name)
        self.desc = unicode(Desc + ':'+ '   ')
        self.url = 'http://bp-manager.ume.maine.edu' + unicode(Url)
        self.channel = unicode(Channel)
        self.update_url = ''

    def update_channel(self, new_channel):
        """Update associated channel"""
        self.channel = new_channel

    def return_update_url(self):
        """Update URL to execute to change program settings"""
        self.update_url = 'http://130.111.228.122/create_allPrograms_programList.html?preventCache=1380651888644&programType=vod&thumbnailId=2439&programId=' + self.id + '&programName=' + urllib.quote_plus(self.name).replace('+', '%20')+ '&description=' +urllib.quote_plus(self.desc).replace('+', '%20')+ '&programCategoryId=11&programChannelId=' +self.channel+ '&themeName=MP4_Video_only&programURL=http%3A%2F%2Fbp-manager.ume.maine.edu%2F' +urllib.quote_plus(self.url[32:])+ '&programVisibility=1&attachmentIds=&programLinks=&publicEvent=true&specialPassword=&confirmPasswd=&embedSilverlight=&action=edit&pStart=0&pNum=20'
        return self.update_url

class LibraryFile(object):
    """Class that works as abstraction for the BP file library elements"""
    def __init__(self, Name, Folder, Owner, ProgramArray, resourceID):
        self.name = Name
        self.folder = Folder
        self.owner = Owner
        self.program_array = ProgramArray
        self.resource_id = resourceID




